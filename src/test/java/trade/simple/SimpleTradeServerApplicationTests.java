package trade.simple;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import trade.simple.controller.SimpleTradeController;
import trade.simple.model.Comment;
import trade.simple.model.Post;
import trade.simple.model.User;
import trade.simple.repo.CommentRepo;
import trade.simple.repo.UserRepo;
import trade.simple.service.CommentService;
import trade.simple.service.PostService;
import trade.simple.service.UserService;

@SpringBootTest
class SimpleTradeServerApplicationTests {
	
	@Mock
	private UserRepo fakeRepo;
	
	@Mock
	private CommentRepo commRepo;
	
	@InjectMocks
	private UserService userServ ; 
	
	@InjectMocks
	private CommentService commServ;
	
	private User user;
	private User user2;
	private SimpleTradeController controller;
	private PostService postServ;
	
	private Comment comment;
	private Post post;
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this); //  will tell mackito to create an instance of ever field member marked by @Mock
		userServ = new UserService(fakeRepo);	
		commServ = new CommentService(commRepo);
		   
		  
		 // controller = new SimpleTradeController(commentServ,postServ,userServ); 
		  user = new User(151,"Roma","teddy.9211@yahoo.com","password","6137001134",new ArrayList<Post>(),new ArrayList<Comment>());
		  post = new Post(1, false, "Car For Sale","selling of my used car, which is almost new", 45, null, 20, null, user, new ArrayList<Comment>());
		  comment = new Comment(8,user,post, "Hey this looks really good I like it.",null);
		  
		  when(fakeRepo.findByUserName("Roma")).thenReturn(user);
		  when(fakeRepo.findByUserId(151)).thenReturn(user);
		  when(commRepo.findByCommentId(8)).thenReturn(comment);
		
	}
	
	
	@Test
	public void getUserBynameSucess() {
		assertEquals("Roma", userServ.getUserByName("Roma").getUserName());
		}
	
	@Test
	public void getUserByIdSucess() {
		assertEquals(151, userServ.getUserById(151).getUserId());
	}
	
	@Test
	public void getCommentByIdSucess() {
		assertEquals(8, commServ.getCommentById(8).getCommentId());
		}
	

}
