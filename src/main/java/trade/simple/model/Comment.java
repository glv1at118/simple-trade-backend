package trade.simple.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class Comment {
	@Id
	@Column(name = "comment_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int commentId;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User userHolder;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinColumn(name = "post_id")
	private Post postHolder;

	@Column(name = "text", nullable = false)
	private String text;

	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;

	public Comment() {
	}

	public Comment(User userHolder, Post postHolder, String text, Timestamp dateCreated) {
		super();
		this.userHolder = userHolder;
		this.postHolder = postHolder;
		this.text = text;
		this.dateCreated = dateCreated;
	}

	public Comment(int commentId, User userHolder, Post postHolder, String text, Timestamp dateCreated) {
		super();
		this.commentId = commentId;
		this.userHolder = userHolder;
		this.postHolder = postHolder;
		this.text = text;
		this.dateCreated = dateCreated;
	}

	public User getUserHolder() {
		return userHolder;
	}

	public void setUserHolder(User userHolder) {
		this.userHolder = userHolder;
	}

	public Post getPostHolder() {
		return postHolder;
	}

	public void setPostHolder(Post postHolder) {
		this.postHolder = postHolder;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getCommentId() {
		return commentId;
	}

	public String toString() {
		return "Comment [commentId=" + commentId + ", userHolder=" + userHolder + ", postHolder=" + postHolder
				+ ", text=" + text + ", dateCreated=" + dateCreated + "]";
	}

	public void nullifyUserHolder() {
		this.userHolder.nullifyCommentList();
		this.userHolder.nullifyPostList();
	}

	public void nullifyPostHolder() {
		this.postHolder.nullifyCommentList();
		this.postHolder.nullifyUserHolder();
	}
}
