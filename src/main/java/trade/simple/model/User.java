package trade.simple.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userId;

	@Column(name = "username", unique = true, nullable = false)
	private String userName;

	@Column(name = "email", unique = true, nullable = false)
	private String email;

	@Column(name = "password_hashed", nullable = false)
	private String passwordHashed;

	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;

	@OneToMany(mappedBy = "userHolder", fetch = FetchType.LAZY)
	private List<Post> postList = new ArrayList<>();

	@OneToMany(mappedBy = "userHolder", fetch = FetchType.LAZY)
	private List<Comment> commentList = new ArrayList<>();

	public User() {
	}

	public User(int userId, String userName, String email, String passwordHashed, String phoneNumber,
			List<Post> postList, List<Comment> commentList) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.email = email;
		this.passwordHashed = passwordHashed;
		this.phoneNumber = phoneNumber;
		this.postList = postList;
		this.commentList = commentList;
	}

	public User(String userName, String email, String passwordHashed, String phoneNumber, List<Post> postList,
			List<Comment> commentList) {
		super();
		this.userName = userName;
		this.email = email;
		this.passwordHashed = passwordHashed;
		this.phoneNumber = phoneNumber;
		this.postList = postList;
		this.commentList = commentList;
	}
	
	public User(String userName, String email, String passwordHashed, String phoneNumber) {
		super();
		this.userName = userName;
		this.email = email;
		this.passwordHashed = passwordHashed;
		this.phoneNumber = phoneNumber;

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHashed() {
		return passwordHashed;
	}

	public void setPasswordHashed(String passwordHashed) {
		this.passwordHashed = passwordHashed;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Post> getPostList() {
		return postList;
	}

	public void setPostList(List<Post> postList) {
		this.postList = postList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", email=" + email + ", passwordHashed="
				+ passwordHashed + ", phoneNumber=" + phoneNumber + ", postList=" + postList + ", commentList="
				+ commentList + "]";
	}
	
	public void nullifyPostList() {
		this.postList = null;
	}
	
	public void nullifyCommentList() {
		this.commentList = null;
	}

}
