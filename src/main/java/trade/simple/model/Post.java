package trade.simple.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "posts")
public class Post {

	@Id
	@Column(name = "post_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int postId;

	@Column(name = "availability", nullable = false)
	private boolean availability;

	@Column(name = "post_title", nullable = false)
	private String postTitle;

	@Column(name = "post_description", nullable = false)
	private String postDescription;
	
	@Column(name = "price", nullable = false)
	private double price;

	@Column(name = "date_created", nullable = false)
	private Timestamp dateCreated;

	@Column(name = "number_of_visits", nullable = false)
	private int numberOfVisits;

	
	@Column(name = "image_url",nullable = false)
	private String imageURL;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	private User userHolder;

	@OneToMany(mappedBy = "postHolder", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	private List<Comment> commentList = new ArrayList<>();

	public Post() {
	}

	public Post(int postId, boolean availability, String postTitle, String postDescription, double price,
			Timestamp dateCreated, int numberOfVisits, String imageURL, User userHolder, List<Comment> commentList) {
		super();
		this.postId = postId;
		this.availability = availability;
		this.postTitle = postTitle;
		this.postDescription = postDescription;
		this.price = price;
		this.dateCreated = dateCreated;
		this.numberOfVisits = numberOfVisits;
		this.imageURL = imageURL;
		this.userHolder = userHolder;
		this.commentList = commentList;
	}

	public Post(boolean availability, String postTitle, String postDescription, double price, Timestamp dateCreated,
			int numberOfVisits, String imageURL, User userHolder, List<Comment> commentList) {
		super();
		this.availability = availability;
		this.postTitle = postTitle;
		this.postDescription = postDescription;
		this.price = price;
		this.dateCreated = dateCreated;
		this.numberOfVisits = numberOfVisits;
		this.imageURL = imageURL;
		this.userHolder = userHolder;
		this.commentList = commentList;
	}
	
	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostDescription() {
		return postDescription;
	}

	public void setPostDescription(String postDescription) {
		this.postDescription = postDescription;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getNumberOfVisits() {
		return numberOfVisits;
	}

	public void setNumberOfVisits(int numberOfVisits) {
		this.numberOfVisits = numberOfVisits;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public User getUserHolder() {
		return userHolder;
	}
	
	public void setUserHolder(User userHolder) {
		this.userHolder = userHolder;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	@Override
	public String toString() {
		return "Post [postId=" + postId + ", availability=" + availability + ", postTitle=" + postTitle
				+ ", postDescription=" + postDescription + ", price=" + price + ", dateCreated=" + dateCreated
				+ ", numberOfVisits=" + numberOfVisits + ", imageURL=" + imageURL + ", userHolder=" + userHolder
				+ ", commentList=" + commentList + "]";
	}

	public void nullifyUserHolder() {
		this.userHolder.nullifyCommentList();
		this.userHolder.nullifyPostList();
	}
	
	public void nullifyCommentList() {
		this.commentList = null;
	}

}
