package trade.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleTradeServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleTradeServerApplication.class, args);
	}

}
