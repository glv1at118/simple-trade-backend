package trade.simple.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import trade.simple.model.Comment;
import trade.simple.model.Post;
import trade.simple.model.User;
import trade.simple.service.AwsS3BucketService;
import trade.simple.service.CommentService;
import trade.simple.service.EmailService;
import trade.simple.service.PostService;
import trade.simple.service.UserService;
import trade.simple.validator.CommentValidator;
import trade.simple.validator.PostValidator;
import trade.simple.validator.UserValidator;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "*")
public class SimpleTradeController {

	private CommentService cServ;
	private PostService pServ;
	private UserService uServ;

	@Autowired
	private EmailService eServ;

	@Autowired
	private AwsS3BucketService amazonS3BucketService;

	@InitBinder("user")
	protected void initBinderU(WebDataBinder userBinder) {
		userBinder.setValidator(new UserValidator());
	}

	@InitBinder("post")
	protected void initBinderP(WebDataBinder postBinder) {
		postBinder.setValidator(new PostValidator());
	}

	@InitBinder("comment")
	protected void initBinderC(WebDataBinder commentBinder) {
		commentBinder.setValidator(new CommentValidator());
	}

	public SimpleTradeController() {

	}

	@Autowired
	public SimpleTradeController(CommentService cServ, PostService pServ, UserService uServ) {
		super();
		this.cServ = cServ;
		this.pServ = pServ;
		this.uServ = uServ;
	}

	@GetMapping()
	public ResponseEntity<String> apiPage() {
		return new ResponseEntity<>("api home page", HttpStatus.OK);
	}

	@GetMapping("/user/{username}") // returns user model object
	public ResponseEntity<User> getUserByName(@PathVariable("username") String username) {
		User user = uServ.getUserByName(username);
		if (user == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		user.nullifyPostList();
		user.nullifyCommentList();

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@GetMapping("/user/posts/{userId}") // returns list of post objects that belong to userId
	public ResponseEntity<List<Post>> getPostsByUser(@PathVariable("userId") int userId) {
		User user = uServ.getUserById(userId);
		if (user == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		List<Post> postList = pServ.getPostsOfAUser(user);
		if (postList == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		for (Post p : postList) {
			p.nullifyCommentList();
			p.nullifyUserHolder();
		}

		return new ResponseEntity<List<Post>>(postList, HttpStatus.OK);
	}

	@GetMapping("/post/{postId}") // returns post model object
	public ResponseEntity<Post> getPostById(@PathVariable("postId") int id) {
		Post post = pServ.getPostByPostId(id);
		if (post == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		post.nullifyCommentList();
		post.nullifyUserHolder();
		return new ResponseEntity<Post>(post, HttpStatus.OK);
	}

	@GetMapping("/post/all") // return list of all posts
	public ResponseEntity<List<Post>> getAllPosts() {
		List<Post> posts = pServ.getAllPosts();
		for (Post p : posts) {
			p.nullifyCommentList();
			p.nullifyUserHolder();
		}
		return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
	}

	@GetMapping("/post/comments/{postId}") // returns list of comment objects that belongs to postId
	public ResponseEntity<List<Comment>> getCommentsByPost(@PathVariable("postId") int id) {
		Post post = pServ.getPostByPostId(id);
		if (post == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		List<Comment> commentList = cServ.getCommentsByPost(post);
		if (commentList == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		for (Comment c : commentList) {
			c.nullifyPostHolder();
			c.nullifyUserHolder();
		}
		return new ResponseEntity<List<Comment>>(commentList, HttpStatus.OK);
	}

	@GetMapping("/post/click/{postId}") // count clicks
	public void incrementClick(@PathVariable("postId") int id) {
		Post clickedPost = pServ.getPostByPostId(id);
		clickedPost.setNumberOfVisits(clickedPost.getNumberOfVisits() + 1);
		// Do NOT update the date created
//		Timestamp date = clickedPost.getDateCreated();
//		clickedPost.setDateCreated(date);
		pServ.upsertPost(clickedPost);
//		return new ResponseEntity<String>("NumberOfVisits Updated", HttpStatus.OK);
	}

	@GetMapping("/comment/{commentId}") // returns comment object
	public ResponseEntity<Comment> getCommentById(@PathVariable("commentId") int id) {
		Comment comment = cServ.getCommentById(id);
		if (comment == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		comment.nullifyPostHolder();
		comment.nullifyUserHolder();
		return new ResponseEntity<Comment>(comment, HttpStatus.OK);
	}

//		{
//		"userName": "ham",
//		"email":"ham",
//		"passwordHashed":"ham",
//		"phoneNumber":"123-432-5234"
//		}

//	use the above to test creating a new user
	@PostMapping("/user") // create new user
	public ResponseEntity<String> insertUser(@RequestBody @Valid User user, BindingResult result) {
		if (result.hasErrors()) {
			System.out.println(result.getFieldError());
			return new ResponseEntity<>(
					result.getFieldError().getCode() + " " + result.getFieldError().getDefaultMessage(),
					HttpStatus.NOT_ACCEPTABLE);
		}
		uServ.upsertUser(user);
		return new ResponseEntity<String>("user created", HttpStatus.CREATED);
	}

	@PostMapping("/user/auth")
	public ResponseEntity<User> authUser(@RequestBody @Valid User user, BindingResult result) {
		User userFound = uServ.getUserByName(user.getUserName());

		if (userFound == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		userFound.nullifyPostList();
		userFound.nullifyCommentList();

		String hashedPasswordFound = userFound.getPasswordHashed();
		String hasedPassword = user.getPasswordHashed();

		boolean isAuth = uServ.checkPass(hasedPassword, hashedPasswordFound);

		if (isAuth == false) {
			return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<User>(userFound, HttpStatus.OK);
	}

	@PostMapping("/post") // create new post
	public ResponseEntity<String> insertPost(@RequestBody @Valid Post post, BindingResult result) {
		// Sets current timestamp that works with postgres insertion.
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		LocalDateTime localDt = LocalDateTime.ofInstant(Instant.ofEpochMilli(ts.getTime()), ZoneOffset.UTC);
		post.setDateCreated(new Timestamp(localDt.toInstant(ZoneOffset.UTC).toEpochMilli()));

		// ensures entry is valid
		if (result.hasErrors()) {
			System.out.println(result.getFieldError());
			return new ResponseEntity<>(
					result.getFieldError().getCode() + " " + result.getFieldError().getDefaultMessage(),
					HttpStatus.NOT_ACCEPTABLE);
		}
		pServ.upsertPost(post);
		return new ResponseEntity<String>("post created", HttpStatus.CREATED);
//		return null;
	}

	
	private int users = 0;
	private User from = null;
	private User to = null;
	@PostMapping("/post/{postId}") // sends interest email
	public ResponseEntity<String> sendEmail(@PathVariable("postId") int id, @RequestBody User user) {
		Post post = pServ.getPostByPostId(id);
		if (post == null) {
			return new ResponseEntity<>("Post doesn't exist", HttpStatus.NOT_FOUND);
		}
		if (users == 0) {
			from = user;
			users++;
			return new ResponseEntity<String>("First user info retrieved", HttpStatus.CREATED);
		} else {
			to = user;
			users=0;
			eServ.sendMail(from, to, id);
			from=null; to=null;
			return new ResponseEntity<String>("Email sent", HttpStatus.CREATED);
		}
	}

	@PostMapping("/comment") // create new comment
	public ResponseEntity<String> insertComment(@RequestBody @Valid Comment comment, BindingResult result) {
		// Sets current timestamp that works with postgres insertion.
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		LocalDateTime localDt = LocalDateTime.ofInstant(Instant.ofEpochMilli(ts.getTime()), ZoneOffset.UTC);
		comment.setDateCreated(new Timestamp(localDt.toInstant(ZoneOffset.UTC).toEpochMilli()));

		// ensures entry is valid
		if (result.hasErrors()) {
			System.out.println(result.getFieldError());
			System.out.println(comment.getDateCreated());
			return new ResponseEntity<>(
					result.getFieldError().getCode() + " " + result.getFieldError().getDefaultMessage(),
					HttpStatus.NOT_ACCEPTABLE);
		}
		cServ.upsertComment(comment);
		return new ResponseEntity<String>("comment created", HttpStatus.CREATED);
	}

	// deletes the user from the database, make sure the session is cleared if you
	// use this so the user does not stay logged in
	@DeleteMapping("/user/{userId}")
	public ResponseEntity<String> deleteUserById(@PathVariable("userId") int id) {
		uServ.deleteUser(uServ.getUserById(id));
		return new ResponseEntity<>("user Deleted", HttpStatus.GONE);
	}

	@DeleteMapping("/post/{postId}")
	public ResponseEntity<String> deletePostById(@PathVariable("postId") int id) {
		pServ.deletePost(pServ.getPostByPostId(id));
		return new ResponseEntity<>("post Deleted", HttpStatus.GONE);
	}

	@DeleteMapping("/comment/{commentId}")
	public ResponseEntity<String> deleteCommentById(@PathVariable("commentId") int id) {
		cServ.deleteCommentById(id);
		return new ResponseEntity<>("comment Deleted", HttpStatus.GONE);
	}

	// controller method to handle the file upload request from angular side.
	@PostMapping("/upload")
	public String uploadFile(@RequestPart(value = "file") MultipartFile file) {
		return this.amazonS3BucketService.uploadFile(file);
	}

//    @PostMapping("/deleteFile")
//    public String deleteFile(@RequestBody String fileURL) {
//        return this.amazonS3BucketService.deleteFileFromBucket(fileURL);
//    }

}
