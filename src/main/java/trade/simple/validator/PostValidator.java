package trade.simple.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import trade.simple.model.Post;

public class PostValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Post.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "postTitle", "postTitle must have value");
	    ValidationUtils.rejectIfEmpty(errors, "postDescription", "postDescription must have value");
		ValidationUtils.rejectIfEmpty(errors, "numberOfVisits", "numberOfVisits must have value");
		ValidationUtils.rejectIfEmpty(errors, "imageURL", "imageURL must have value");	
	}

}
