package trade.simple.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


import trade.simple.model.User;
public class UserValidator implements Validator{

	//Ensures validator works for User class
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "userName", "userName must have value");
		ValidationUtils.rejectIfEmpty(errors, "email", "email must have value");
		ValidationUtils.rejectIfEmpty(errors, "passwordHashed", "password must have value");
		ValidationUtils.rejectIfEmpty(errors, "phoneNumber", "phoneNumber must have value");	
	}
}
