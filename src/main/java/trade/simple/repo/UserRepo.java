package trade.simple.repo;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import trade.simple.model.User;

public interface UserRepo extends JpaRepository<User, Integer> {

	
	public List<User> findAll();
	public User findByUserName(String userName);
	public User findByUserId(int userId);
}
