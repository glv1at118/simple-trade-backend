package trade.simple.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import trade.simple.model.Post;
import trade.simple.model.User;

public interface PostRepo extends JpaRepository<Post, Integer> {

	public Post findByPostId(int postId);
	
	public List<Post> findAllByOrderByPostIdDesc();
	public List<Post> findByUserHolder(User user);
	public List<Post> findByUserHolderOrderByPostIdDesc(User user);
	public List<Post> findByUserHolderOrderByPostIdAsc(User user);
	
}
