package trade.simple.repo;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import trade.simple.model.Comment;
import trade.simple.model.Post;
import trade.simple.model.User;

public interface CommentRepo extends JpaRepository<Comment, Integer> {
	public Comment findByCommentId(int commentId);
	public Comment findByDateCreated(Timestamp dateCreated);
	
	
	public List<Comment> findAll();
	public List<Comment> findByPostHolderOrderByCommentIdDesc(Post post);
	public List<Comment> findByUserHolder(User user);
}
