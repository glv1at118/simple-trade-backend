package trade.simple.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Service
public class AwsS3BucketService {

	private AmazonS3 amazonS3;
	public final static Logger log = Logger.getLogger(AwsS3BucketService.class);

	@Value("${endpointUrl}")
	private String endpointUrl;
	@Value("${bucketName}")
	private String bucketName;
	@Value("${accessKey}")
	private String accessKey;
	@Value("${secretKey}")
	private String secretKey;

	@SuppressWarnings("deprecation")
	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		this.amazonS3 = new AmazonS3Client(credentials);
	}

	public String uploadFile(MultipartFile multipartFile) {
		String fileURL = "";
		try {
			File file = this.convertMultipartFileToFile(multipartFile);
			String fileName = multipartFile.getOriginalFilename();
			fileURL = endpointUrl + "/" + bucketName + "/" + fileName;
			this.uploadFileToBucket(fileName, file);
			file.delete();
			log.info("File uploaded to Bucket");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return fileURL;
	}

	private File convertMultipartFileToFile(MultipartFile file) throws IOException {
		File convertedFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convertedFile);
		fos.write(file.getBytes());
		fos.close();
		log.info("Multi-part file converted");
		return convertedFile;
	}

	private void uploadFileToBucket(String fileName, File file) {
		log.info("File uploaded to S3 bucket");
		this.amazonS3.putObject(
				new PutObjectRequest(bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
	}

//	public String deleteFileFromBucket(String fileName) {
//		this.amazonS3.deleteObject(new DeleteObjectRequest(bucketName, fileName));
//		return "Deletion Successful";
//	}

}