package trade.simple.service;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trade.simple.model.Comment;
import trade.simple.model.Post;
import trade.simple.model.User;
import trade.simple.repo.CommentRepo;

@Service
public class CommentService {

	private CommentRepo cRepo;
	public final static Logger log = Logger.getLogger(CommentService.class);

	public CommentService() {

	}
	
	@Autowired
	public CommentService(CommentRepo cRepo) {
		this.cRepo = cRepo;
	}


	public Comment getCommentById(int id) {
		log.info("Comment retrieved by id");
		return this.cRepo.findByCommentId(id);
	}

	public Comment getCommentByDate(Timestamp dateCreated) {
		log.info("Comment retrieved by date");
		return this.cRepo.findByDateCreated(dateCreated);
	}


	public Comment upsertComment(Comment newComment) {
		log.info("Comment inserted into database");
		return this.cRepo.save(newComment);
	}

	public void deleteCommentById(int id) {
		log.info("Comment deleted with ID:"+id);
		this.cRepo.deleteById(id);
	}

	public void deleteCommentByEntity(Comment c) {
		log.info("Comment deleted with ID:"+c.getCommentId());
		this.cRepo.delete(c);
	}
	
	
	public List<Comment> getAllComments() {
		log.info("All comments retrieved");
		return this.cRepo.findAll();
	}
	
	public List<Comment> getCommentsByPost(Post post){
		log.info("Comments retrieved by post");
		return cRepo.findByPostHolderOrderByCommentIdDesc(post);
	}
	
	public List<Comment> getCommentsByUser(User user){
		log.info("Comments retrieved by user");
		return cRepo.findByUserHolder(user);
	}

}
