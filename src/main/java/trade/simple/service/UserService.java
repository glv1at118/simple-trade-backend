package trade.simple.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import trade.simple.model.User;
import trade.simple.repo.UserRepo;


@Service
public class UserService {
	
	private UserRepo uRepo;
	public final static Logger log = Logger.getLogger(UserService.class);
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public UserService(UserRepo uRepo) {
		super();
		this.uRepo = uRepo;
	}
	
	public void upsertUser(User user) {
        user.setPasswordHashed(hashPassword(user.getPasswordHashed()));
        log.info("New user inserted with username:"+user.getUserName());
        uRepo.save(user);
    }
	private String hashPassword(String plainTextPassword){
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }

	public boolean checkPass(String plainPassword, String hashedPassword) {
        if (BCrypt.checkpw(plainPassword, hashedPassword))
            return true;
        else
            return false;
    }
	
	public List<User> getAllUsers(){
		log.info("All users retrieved");
		return uRepo.findAll();
	}
	
	public User getUserByName(String name) {
		log.info("User retrieved by name:"+name);
		return uRepo.findByUserName(name);
	}
	
	public User getUserById(int userId) {
		log.info("User retrieved by ID:"+userId);
		return uRepo.findByUserId(userId);
	}
	
	public void deleteUser(User user) {
		log.info("User deleted with ID:"+user.getUserId());
		uRepo.delete(user);
	}

}
