package trade.simple.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import trade.simple.model.Post;
import trade.simple.model.User;
import trade.simple.repo.PostRepo;

@Service
public class PostService {
	
	private PostRepo postRepo;
	public final static Logger log = Logger.getLogger(PostService.class);
	
	public PostService() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public PostService(PostRepo postRepo) {
		super();
		this.postRepo = postRepo;
	}
	
	public List<Post> getAllPosts() {
		log.info("All posts retrieved");
		return postRepo.findAllByOrderByPostIdDesc();
	}
	
	public List<Post> getPostsOfAUser(User user) {
		log.info("All posts retrieved for user ID:"+user.getUserId());
		return postRepo.findByUserHolderOrderByPostIdDesc(user);
	}
	
	public void upsertPost(Post post) {
		log.info("Post inserted into database");
		postRepo.save(post);
	}
	
	
	public Post getPostByPostId(int postId) {
		log.info("Post retrieved by ID:"+postId);
		return postRepo.findByPostId(postId);
	}
	
	
	public void deletePost(Post post) {
		log.info("Post deleted with ID:"+post.getPostId());
		postRepo.delete(post);
	}

}
