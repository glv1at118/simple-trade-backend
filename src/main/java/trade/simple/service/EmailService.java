package trade.simple.service;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import trade.simple.model.User;

@Service
public class EmailService {
    private JavaMailSender mailSender;
    public final static Logger log = Logger.getLogger(EmailService.class);
 
    @Autowired
    public EmailService(JavaMailSender mailSender)
    {
    	this.mailSender = mailSender;
    }

    public void sendMail(User from, User to, int id){
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(to.getEmail());
        mail.setFrom("bymsbo@gmail.com");
        mail.setSubject("Simple Trader Post Inquiry");
        mail.setText("Hello! User "+from.getUserName()+" has expressed interest in your ad with ID:"+id+"! They can be reached at "+from.getEmail());
        try {
        	mailSender.send(mail);
        	log.info("Email sent successfully");
        } catch (MailException e) {
        	log.error(e);
        	e.printStackTrace();
        }
    }
}
