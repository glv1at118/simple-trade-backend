# simple-trader

## Project Description

Simple Trader is an online marketplace platform that provides a convenient way for people to buy and sell things. Anyone can visit the site and browse existing ads, while registered users can view the detailed page of each ad, post their own ads, edit or delete their ads, write comments under any ad, and send emails via Simple Trader to other users. Simple Trader also offers product multi-criteria filtering functionalities that allow users to search for anything they’re interested in. Simple Trader is developed as a single page application that provides dynamic rendering of product updates, live interactive feedback, and a consistent experience for its users.

## Technologies Used

- Java
- Spring
- Angular
- Git
- Spring Boot
- Spring Data
- PostgreSQL
- AWS S3
- JUnit
- Mockito
- Jenkins

## Features

List of features ready and TODOs for future development

- Users can create new accounts, login to an existing account, and logout.
- Users can reset their password.
- Users can upload and display posts attached with an image.
- Users can view details of other users’ postings.
- Users can filter posts based on different criteria.
- Users can save, edit, and delete posts.
- Users can send emails to other users via Java Mail.
- Users can interact with other users by making comments under a post.

To-do list:

- Users can chat with each other real-time via a chat window
- Users can receive notifications if their posts get commented by other users

## Repository Link

For Simple Trader front end: 

```
https://gitlab.com/glv1at118/simple-trade-frontend
```

For Simple Trader back end:

```
https://gitlab.com/glv1at118/simple-trade-backend
```

## Getting Started

If you would like to host the application on a local machine, follow the steps below:

1. Clone the front end and back end repository of Simple Trader:

   ```
   git clone https://gitlab.com/glv1at118/simple-trade-backend.git
   git clone https://gitlab.com/glv1at118/simple-trade-frontend.git
   ```

2. Update the dependencies of both front end and back end of Simple Trader. To update front end dependencies:

   ```
   npm install
   ```

   To update back end dependencies, use maven to update the project based on the pom.xml

3. Start the Tomcat server for the backend and start the front end Angular server:

   ```
   ng serve
   ```

4. Visit the URL below to access the Simple Trader locally:

   ```
   http://localhost:4200/home
   ```

## Usage

To visit the live demo of the application, you can visit:

```
http://guannan-bucket.s3-website.us-east-2.amazonaws.com/home
```

![picture1](/uploads/9add922d31c4620cd664295129747963/picture1.PNG)

![picture_2](/uploads/ec7c7c78acbfe5d8c272cb7df7f0b69c/picture_2.PNG)

![picture_3](/uploads/c57693a18e9e62232263f854fd3cbfa0/picture_3.PNG)

![picture_4](/uploads/54809fc58555e0f99605ade1f753c872/picture_4.PNG)

## Contributors

Mohammad Rafik, Ansleigh Nimako-Boateng, Guannan Lyu, Choongwon Lee, Roma Patel
